package com.example.godaa.linkdevtask.Views.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.godaa.linkdevtask.Adapter.ArticleAdapter;
import com.example.godaa.linkdevtask.Model.Articles;
import com.example.godaa.linkdevtask.Module.RetrofitModule;
import com.example.godaa.linkdevtask.Networking.Interface.RetrofitInterface;
import com.example.godaa.linkdevtask.Presenter.GridMainPresenter;
import com.example.godaa.linkdevtask.R;
import com.example.godaa.linkdevtask.interfaces.IGridView;
import com.example.godaa.linkdevtask.interfaces.onitemclick;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by godaa on 10/11/2017.
 */

public class GridMainFragment extends Fragment implements IGridView {
    ArticleAdapter articleAdapter;
    GridMainPresenter gridMainPresenter;
    @BindString(R.string.base_url)
    String baseApiUrl;
    @BindView(R.id.recycler_swipe_refresh)
    SwipyRefreshLayout swipeListFriends;
    ProgressDialog progressDialog;
    @BindView(R.id.recyleview_task)
    RecyclerView recyleview;
    @BindString(R.string.api_key)
    String api_key;
    @BindString(R.string.source)
    String source;
    RetrofitInterface retrofitInterface;
    Retrofit retrofit;
    onitemclick onitemclick;

    public GridMainFragment() {
        super();

    }

    /* public GridMainFragment newInstance() {
         return new GridMainFragment();
     }
 */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // outState.putParcelable("movie",moviedata);
    }

    public void getArticles() {
        progressDialog.show();
        gridMainPresenter = new GridMainPresenter(this);
        gridMainPresenter.getArticles(retrofitInterface, source, api_key);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grid_fragment, container, false);
        ButterKnife.bind(this, view);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("loading");
        retrofit = RetrofitModule.getRetrofit(baseApiUrl);
        retrofitInterface = RetrofitModule.getRetrofitInterface(retrofit);
        setUpGridRecyclerView();
        getArticles();
        return view;

    }

    private void setUpGridRecyclerView() {
        GridLayoutManager lLayout;
        lLayout = new GridLayoutManager(getActivity(), 1);
        recyleview.setHasFixedSize(true);
        recyleview.setLayoutManager(lLayout);
    }


    @Override
    public void OnSuccessGetData(Response<JsonObject> o) {
        if (o.raw().code() == 200) {
            try {
                progressDialog.dismiss();
                Gson gson = new Gson();
                Articles result = gson.fromJson(o.body(), Articles.class);
                articleAdapter = new ArticleAdapter(getContext(), result.getArticles(), getActivity());
                recyleview.setAdapter(articleAdapter);
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    @Override
    public void OnFailGetData(Throwable o) {
        try {
            Toast.makeText(getContext(), "Failed to load articles", Toast.LENGTH_LONG).show();
        } catch (Exception e) {

            e.getMessage();
        }
        progressDialog.dismiss();
    }
}
