package com.example.godaa.linkdevtask.Networking.Parser;

import com.example.godaa.linkdevtask.Networking.Interface.CallBackJSONObject;
import com.example.godaa.linkdevtask.Networking.Interface.RetrofitInterface;
import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//=================================================================
// this class contains all functions with return type JsonObject
//=================================================================
public class JSONObjectParser implements Callback<JsonObject> {

    private final String baseURL;
    CallBackJSONObject callBackJSONObject;

    //=================================================================
    // constructor for creating calling object for network instance
    //=================================================================
    public JSONObjectParser(String baseUrl, CallBackJSONObject call) {
        this.callBackJSONObject = call;
        this.baseURL = baseUrl;
    }

    //=================================================================
    // below is all web api methods with return type jsonObject
    //=================================================================

    public void getArticlesData(RetrofitInterface inter,String source,String apiKey) {
        Call<JsonObject> resString = inter.getData("articles",source,apiKey);
        resString.enqueue(this);
    }



    @Override
    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
        callBackJSONObject.onSuccess(response);
    }
    //=================================================================
    // Response CallBack for error response
    //=================================================================
    @Override
    public void onFailure(Call<JsonObject> call, Throwable t) {
        callBackJSONObject.OnFail(t);
    }
}
