package com.example.godaa.linkdevtask.Networking.Interface;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RetrofitInterface {


    @Headers({
            "content-type: application/json"})
    @GET("v1/{articles}/")
    Call<JsonObject> getData(@Path("articles") String articles, @Query("source") String source,@Query("apiKey") String apiKey);

}