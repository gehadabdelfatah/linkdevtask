package com.example.godaa.linkdevtask.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.godaa.linkdevtask.Model.Article;
import com.example.godaa.linkdevtask.R;
import com.example.godaa.linkdevtask.interfaces.onitemclick;
import com.example.godaa.linkdevtask.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by godaa on 10/11/2017.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleAdapterHolder> {
    Context mContext;
    List<Article> articles = new ArrayList<>();
    onitemclick onitemclick;
    Picasso picassoobj;

    public ArticleAdapter(Context context, List<Article> articles, Context onitemclick) {
        mContext = context;
        this.onitemclick = (onitemclick) onitemclick;
        this.articles = articles;
    }

    @Override
    public ArticleAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.list_item, parent, false);

        return new ArticleAdapterHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ArticleAdapterHolder holder, int position) {
        final Article article = articles.get(position);
        holder.author.setText("By " + article.getAuthor());

        holder.date.setText(Utils.FormattedDate(article.getPublishedAt()));
        holder.title.setText(article.getTitle());
        picassoobj.with(mContext)
                .load(article.getUrlToImage())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.imageView);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onitemclick.OnItemSelcted(article);
            }
        });
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class ArticleAdapterHolder extends RecyclerView.ViewHolder {
        View mView;

        @BindView(R.id.author)
        TextView author;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.image_article)
        ImageView imageView;

        public ArticleAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }
    }
}
