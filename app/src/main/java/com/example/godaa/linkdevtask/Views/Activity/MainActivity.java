package com.example.godaa.linkdevtask.Views.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.godaa.linkdevtask.Model.Article;
import com.example.godaa.linkdevtask.R;
import com.example.godaa.linkdevtask.Views.Fragment.GridMainFragment;
import com.example.godaa.linkdevtask.interfaces.onitemclick;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements onitemclick, NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    String Grid_tag = "grid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();
        //for when click arrow in navigation view close the drawer layout
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerview = navigationView.getHeaderView(0);
        ImageView arrowIMage = (ImageView) headerview.findViewById(R.id.arrowpic);
        arrowIMage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        loadMainFragment();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void loadMainFragment() {
        //  menuOrback.setImageResource(R.drawable.menu);
        //for if gridFragment already exist or created
        //then you shouldnt try to recreate it
        if (getSupportFragmentManager().findFragmentByTag(Grid_tag) != null) {
            GridMainFragment gridMainFragment = (GridMainFragment) getSupportFragmentManager().findFragmentByTag(Grid_tag);
            //  Detailfragment.newInstance(date);
            gridMainFragment.getArticles();
        } else {

            getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment
                    , new GridMainFragment(), Grid_tag).commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
      /*  if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    public void message(int id) {
        if (id == R.id.nav_explore) {
            Toast.makeText(this, "EXPLORE", Toast.LENGTH_LONG).show();
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            Toast.makeText(this, "GALLERY", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_livechat) {
            Toast.makeText(this, "LIVE_CHAT", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_wishlists) {
            Toast.makeText(this, "WISH_LISTS", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_magazine) {
            Toast.makeText(this, "E_MAGAZINE", Toast.LENGTH_LONG).show();

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        message(id);

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    void loadDetailActivity(Article article) {
        //  menuOrback.setImageResource(R.drawable.ic_action_back);
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("article", article);

        startActivity(intent);

    }


    @Override
    public void OnItemSelcted(Article article) {
        loadDetailActivity(article);
    }
}
