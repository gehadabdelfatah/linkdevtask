package com.example.godaa.linkdevtask.Presenter;

import com.example.godaa.linkdevtask.Networking.Interface.CallBackJSONObject;
import com.example.godaa.linkdevtask.Networking.Interface.RetrofitInterface;
import com.example.godaa.linkdevtask.Networking.Parser.JSONObjectParser;
import com.example.godaa.linkdevtask.R;
import com.example.godaa.linkdevtask.interfaces.IGridView;
import com.google.gson.JsonObject;

import butterknife.BindString;
import retrofit2.Response;

/**
 * Created by godaa on 11/11/2017.
 */

public class GridMainPresenter implements CallBackJSONObject {
    IGridView iGridView;

    public GridMainPresenter(IGridView iGridView) {
        super();
        this.iGridView = iGridView;
    }

    @BindString(R.string.base_url)
    String baseApiUrl;

    public void getArticles(RetrofitInterface retrofitInterface, String source, String api_key) {
        JSONObjectParser getArticle = new JSONObjectParser(baseApiUrl, this);
        getArticle.getArticlesData(retrofitInterface, source, api_key);
    }

    @Override
    public void onSuccess(Response<JsonObject> o) {
        iGridView.OnSuccessGetData(o);
    }

    @Override
    public void OnFail(Throwable o) {
        iGridView.OnFailGetData(o);
    }
}
