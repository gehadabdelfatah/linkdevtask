package com.example.godaa.linkdevtask.Views.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.godaa.linkdevtask.Model.Article;
import com.example.godaa.linkdevtask.R;
import com.example.godaa.linkdevtask.Views.Fragment.DetailArticleFragment;

/**
 * Created by godaa on 11/11/2017.
 */

public class DetailActivity extends AppCompatActivity {
    String Detail_tag = "detail_fragment";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        Article article = getIntent().getExtras().getParcelable("article");
        if (getSupportFragmentManager().findFragmentByTag(Detail_tag) != null) {
            DetailArticleFragment detailArticleFragment = (DetailArticleFragment) getSupportFragmentManager().findFragmentByTag(Detail_tag);
            //  Detailfragment.newInstance(date);
            //DetailArticleFragment.getArticles();
        } else {

            getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment
                    , new DetailArticleFragment().newInstance(article), Detail_tag).commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }
}
