package com.example.godaa.linkdevtask.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by godaa on 11/11/2017.
 */

public class Utils {
    //convert date for format mmmm dd, yyyy
    public  static String FormattedDate(String date) {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("MMMM dd,  yyyy");
        date = spf.format(newDate);
        return date;
    }
}
