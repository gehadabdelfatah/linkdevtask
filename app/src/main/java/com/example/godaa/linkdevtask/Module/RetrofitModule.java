package com.example.godaa.linkdevtask.Module;

import com.example.godaa.linkdevtask.Networking.Interface.RetrofitInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by godaa on 10/11/2017.
 */

public class RetrofitModule {
    public static Retrofit getRetrofit(String baseurl) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder().
                addInterceptor(httpLoggingInterceptor)
                .build();
        return new Retrofit.Builder()
                .baseUrl(baseurl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }


    public static RetrofitInterface getRetrofitInterface(Retrofit getRetrofit) {

        return getRetrofit.create(RetrofitInterface.class);
    }

}
