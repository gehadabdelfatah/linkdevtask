package com.example.godaa.linkdevtask.interfaces;

import com.google.gson.JsonObject;

import retrofit2.Response;

/**
 * Created by godaa on 11/11/2017.
 */

public interface IGridView {
    void OnSuccessGetData(Response<JsonObject> o);

    void OnFailGetData(Throwable o);
}
