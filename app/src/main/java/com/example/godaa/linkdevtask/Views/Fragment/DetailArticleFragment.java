package com.example.godaa.linkdevtask.Views.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.godaa.linkdevtask.Model.Article;
import com.example.godaa.linkdevtask.R;
import com.example.godaa.linkdevtask.utils.Utils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by godaa on 10/11/2017.
 */

public class DetailArticleFragment extends Fragment {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.author)
    TextView author;
    @BindView(R.id.publishAt)
    TextView publishAt;
    @BindView(R.id.image_article)
    ImageView imageView;
    @BindView(R.id.description)
    TextView descTextView;
    @BindView(R.id.openWeb)
    Button openWep;
    Picasso picasso;

    public DetailArticleFragment() {
        super();
    }

    public DetailArticleFragment newInstance(Article article) {
        DetailArticleFragment detailArticleFragment = new DetailArticleFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("article", article);
        detailArticleFragment.setArguments(bundle);
        return detailArticleFragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // outState.putParcelable("movie",moviedata);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        Article article = bundle.getParcelable("article");
        setDetails(article);
        return view;
    }

    public void setDetails(final Article article) {
        descTextView.setText(article.getDescription());
        author.setText("By " + article.getAuthor());
        title.setText(article.getTitle());

        publishAt.setText(Utils.FormattedDate(article.getPublishedAt()));
        picasso.with(getActivity())
                .load(article.getUrlToImage())
                .into(imageView);
        openWep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String url = article.getUrl();
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });
    }
}
